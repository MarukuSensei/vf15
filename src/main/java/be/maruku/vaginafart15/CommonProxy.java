package be.maruku.vaginafart15;

import be.maruku.vaginafart15.couffey.ModItems;
import be.maruku.vaginafart15.crafting.ModCrafting;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.common.MinecraftForge;

public class CommonProxy {

    public void preInit(FMLPreInitializationEvent e) {ModItems.init();}
    public void init(FMLInitializationEvent e) {
        ModCrafting.initCrafting();
    }
    public void postInit(FMLPostInitializationEvent e) {
    }
}
