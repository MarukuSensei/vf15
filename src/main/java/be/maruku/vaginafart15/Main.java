package be.maruku.vaginafart15;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.sound.SoundEvent;
import net.minecraftforge.common.MinecraftForge;

import java.util.List;

@Mod(modid = Main.MODID, name = Main.MODNAME, version = Main.VERSION)
public class Main {


    public static final String MODID = "vf";
    public static final String MODNAME = "Vaginafart15";
    public static final String VERSION = "1.0.0-ALPHA";

    @SidedProxy(clientSide = "be.maruku.vaginafart15.ClientProxy", serverSide = "be.maruku.vaginafart15.ServerProxy")
    public static CommonProxy proxy;

    @EventHandler
    public void preInit(FMLPreInitializationEvent e) {
        proxy.preInit(e);
    }

    @EventHandler
    public void init(FMLInitializationEvent e) {
        proxy.init(e);
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent e) {}

}
